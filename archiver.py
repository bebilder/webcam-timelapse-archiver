# this utility prunes old files
# uploads the timelapse files from yesterday to Box

# system libraries
import os, sys, getopt, configparser
from datetime import datetime, timedelta
from pathlib import Path

import shutil
from shutil import rmtree

# python ftp library
import ftplib
from ftplib import Error
import ftputil
import ftputil.session
import ftputil.error
class FTPClient:
    def __init__(self, ftp_host, ftp_user, ftp_pass):
        self.ftp_session = None
        self.ftp = None
        self.host = ftp_host
        self.user = ftp_user
        self.pswd = ftp_pass
    
    def connect(self):
        print("\n== FTP CONNECT ==")
        try:
            # ftp session for ftputil
            self.ftp_session = ftputil.session.session_factory(
                base_class=ftplib.FTP,
                port=21,
                use_passive_mode=True,
                encrypt_data_channel=True,
                debug_level=None)

            # ftplib connection
            self.ftp = ftputil.FTPHost(
                self.host, 
                self.user, 
                self.pswd, 
                session_factory=self.ftp_session)

        except (ftputil.error.FTPError, Error) as e:
            print('FTP error: ', e)

    def close(self):
        if self.ftp != None:
            self.ftp.close()
            self.ftp = None
        else:
            print("Please open a connection first.")
            return

    def upFile(self, ftp_dir, file_path, filename):
        print("\n== FILE UPLOAD ==")
        print("Uploading File: ", file_path)
        print("=> ", ftp_dir + '/' + filename)
        try:
            self.ftp.makedirs(ftp_dir, exist_ok=True)
            self.ftp.chdir(ftp_dir)
            self.ftp.upload(file_path, filename)
        except (ftputil.error.FTPError, Error) as e:
            print('FTP error: ', e)

    def upDir(self, ftp_dir, directory):
        print("\n== DIR UPLOAD ==")
        print("Uploading Dir: ", directory)
        print("=> ", ftp_dir)
        try:
            self.ftp.makedirs(ftp_dir, exist_ok=True)
            self.ftp.chdir(ftp_dir)
            files = sorted(os.listdir(directory))
            os.chdir(directory)
            for item in files:
                if os.path.isfile(directory + '/' + item):
                    self.upFile(ftp_dir, directory + '/' + item, item)
                elif os.path.isdir(directory + '/' + item):
                    self.upDir(ftp_dir + '/' + item, directory + '/' + item)
        except (ftputil.error.FTPError, Error) as e:
            print('FTP error: ', e)

# main function
if __name__ == "__main__":
    # fetch info from the config file
    try:
        opts, args = getopt.getopt(sys.argv[1:], "", ["config="])

    except (Exception, getopt.GetoptError):
        print("Specify a config file with --config=<file name>")
        sys.exit(1)

    if len(opts) < 1:
        print("Specify a file with --config=<file name>")
        sys.exit(1)

    config_file = None

    for o, a in opts:
        if o == "--config":
            config_file = a

    config = configparser.ConfigParser()
    config.read(config_file)

    # current time and date values
    now = datetime.now()
    yesterday = now - timedelta(days=1)
    old_week = now - timedelta(days=7)
    old_year = now - timedelta(days=365)

    print("\n=================================================")
    print("Today's Date: ", now.strftime("%Y-%m-%d"))

    # base path for timelapse files
    base_path = config['GENERAL']['LOCAL_DIR']
    yesterday_path = base_path + yesterday.strftime("/%Y/%m/%d")

    print("Archive Dir:  ", yesterday_path)
    
    # remove files from last week
    old_week_path = base_path + old_week.strftime("/%Y/%m/%d")
    if Path(old_week_path).exists(): 
        print("Removing Dir", old_week_path)
        rmtree(old_week_path)

    # remove files that are a year old
    old_year_path = base_path + old_year.strftime("/%Y/%m/%d")
    if Path(old_year_path).exists(): 
        print("Removing Dir", old_year_path)
        rmtree(old_year_path)

    # move current timelapse to archive
    current_timelapse = 'current.mp4'
    timelapse = 'timelapse.mp4'
    if Path(yesterday_path + '/' + timelapse).exists():
        pass
    else:
        shutil.copyfile(base_path + '/' + current_timelapse, yesterday_path + '/' + timelapse)

    # upload files from previous day
    ftp = FTPClient(
        config['FTP']['SERVER'], 
        config['FTP']['USER'], 
        config['FTP']['PASS'])
    ftp.connect()
    ftp.upDir(config['FTP']['ARCHIVE_DIR'] + yesterday.strftime("/%Y/%m/%d"), yesterday_path)
    ftp.close()

    elapsed_time = datetime.now() - now
    print("\nElapsed Time: ", elapsed_time)
    print("=================================================")

    sys.exit(0)