# Webcam Timelapse Archiver

## Summary

This project constists of 3 different parts:

- snapshot => downloads the current frame from the provided camera url
- timelapse => creates a timelapse video from the snapshot frames that are gathered over time
- archiver => uploads the frames and timelapse video of the previous day to an FTP source

This project is designed for use with HikVision IP cameras and their specific URL endpoints,
but the source can be modified to fit the url scheme of different cameras.

### Examples

Here is an example timelapse that is created: [timelapse.mp4 (56MB)](https://tnc.box.com/shared/static/x9ni424icd7vislid2pnyjregwojq0ib.mp4)

## Using the Project

The entire project is python 3 based, and only requires a few dependencies that can be installed with Pip

### Requirements

- Python 3
- urllib
- ftplib
- ftputil
- b2sdk (if using BackBlaze B2)
- ffmpeg-python

### Usage

For our use case the Python programs/scripts are run as a cron job:

```bash
# timelapse archiver
*   6-19 * * * /usr/bin/python3 /opt/webcam-timelapse-archiver/snapshot.py  --config=/opt/webcam-timelapse-archiver/config.ini >> /opt/webcam-timelapse-archiver/logs/snapshot.log
0   7-20 * * * /usr/bin/python3 /opt/webcam-timelapse-archiver/timelapse.py --config=/opt/webcam-timelapse-archiver/config.ini >> /opt/webcam-timelapse-archiver/logs/timelapse.log
10  0    * * * /usr/bin/python3 /opt/webcam-timelapse-archiver/archiver.py  --config=/opt/webcam-timelapse-archiver/config.ini >> /opt/webcam-timelapse-archiver/logs/archiver.log
```

Note that you will need to specify a config file. An example is provided and should be pretty self explanatory.

Logs are managed by logrotate which is standard on Linux based systems.
An example configuration can be found in the project files.

## Troubleshooting

We are running this project on a Raspberry Pi 4 running Ubuntu Server 20.04 connected via ethernet to a DSL connection.
The project _should_ work with basically any Linux distribution without any problems.
If you run into any issues it will probably be with the FTP upload system since it can be finnicky.
You can always run the programs individually to get console output of what is happening to see failure spots.
If running with a cron job like we are you can check the logs also.
