# this utility takes snapshots from an IP camera

# system libraries
import os, sys, getopt, configparser
from datetime import datetime, date
from pathlib import Path
import shutil

# imports for url and http requests
import urllib, urllib.parse, urllib.request
from urllib.error import URLError, HTTPError
# imports for hashing
from base64 import b64encode

# python ftp library
import ftplib
from ftplib import Error
import ftputil
import ftputil.session
import ftputil.error
class FTPClient:
    def __init__(self, ftp_host, ftp_user, ftp_pass):
        self.ftp_session = None
        self.ftps = None
        self.host = ftp_host
        self.user = ftp_user
        self.pswd = ftp_pass
    
    def connect(self):
        print("\n== FTP CONNECT ==")
        try:
            # ftp session for ftputil
            self.ftp_session = ftputil.session.session_factory(
                base_class=ftplib.FTP,
                port=21,
                use_passive_mode=True,
                encrypt_data_channel=True,
                debug_level=None)

            # ftplib connection
            self.ftps = ftputil.FTPHost(
                self.host, 
                self.user, 
                self.pswd, 
                session_factory=self.ftp_session)

        except (ftputil.error.FTPError, Error) as e:
            print('FTP error: ', e)

    def close(self):
        if self.ftps != None:
            self.ftps.close()
            self.ftps = None
        else:
            print("Please open a connection first.")
            return

    def upFile(self, ftp_dir, file_path, filename):
        print("\n== FILE UPLOAD ==")
        print("Uploading File: ", file_path)
        print("=> ", ftp_dir + '/' + filename)
        try:
            self.ftps.makedirs(ftp_dir, exist_ok=True)
            self.ftps.chdir(ftp_dir)
            self.ftps.upload(file_path, filename)
        except (ftputil.error.FTPError, Error) as e:
            print('FTP error: ', e)

# backblaze b2 file storage
from b2sdk.v1 import B2Api, InMemoryAccountInfo, TqdmProgressListener
def b2Upload(key_id, key, bucket_name, file_path, file_name):
    print("\n== B2 LOG ==")

    # authorize account
    info = InMemoryAccountInfo()  # store credentials, tokens and cache in memory
    b2 = B2Api(info)
    b2.authorize_account("production", key_id, key)

    # set up a progress listener
    file_size = Path(file_path).stat().st_size
    print("File Size:", file_name, f"{file_size / 1024 : .2f}", "KB")
    progress = TqdmProgressListener("B2 Upload Progress")
    progress.set_total_bytes(file_size)

    # upload file to bucket
    bucket = b2.get_bucket_by_name(bucket_name)
    bucket.upload_local_file(
        local_file=file_path,
        file_name=file_name,
        progress_listener=progress)
    url = bucket.get_download_url(file_name)
    print("Download URL: ", url)

# main function
if __name__ == "__main__":
    # fetch info from the config file
    try:
        opts, args = getopt.getopt(sys.argv[1:], "", ["config="])

    except (Exception, getopt.GetoptError):
        print("Specify a config file with --config=<file name>")
        sys.exit(1)

    if len(opts) < 1:
        print("Specify a file with --config=<file name>")
        sys.exit(1)

    config_file = None

    for o, a in opts:
        if o == "--config":
            config_file = a

    config = configparser.ConfigParser()
    config.read(config_file)

    # current time and date values
    now = datetime.now()

    print("\n=================================================")
    print("Run Time: ", now.strftime("%Y-%m-%d %H:%M"))

    # make directory structure as needed
    base_path = config['GENERAL']['LOCAL_DIR']
    current_path = base_path + now.strftime("/%Y/%m/%d")
    Path(current_path + "/frames").mkdir(parents=True, exist_ok=True)

    print("Save Dir: ", current_path)

    # remove already existing image
    snapshot = "current.jpg"
    snap_path = base_path + "/" + snapshot
    if Path(snap_path).exists(): Path(snap_path).unlink()

    # get a new snapshot from the camera
    snap_url = config['CAMERA']['URL_IMG']
    auth_bool = config['CAMERA']['AUTH']
    auth_user = config['CAMERA']['AUTH_USER']
    auth_pass = config['CAMERA']['AUTH_PASS']

    try:
        request = urllib.request.Request(snap_url)
        base64string = b64encode(bytes('%s:%s' % (auth_user, auth_pass),'ascii'))
        request.add_header("Authorization", "Basic %s" % base64string.decode('utf-8'))
        data = urllib.request.urlopen(request)

    except (Exception, HTTPError, URLError) as error :
        print ("Error connecting to camera! => ", error)
        sys.exit(2)

    print("Current:  ", snap_path)

    # write the snapshot data to file
    try:
        f = open(snap_path, 'wb')
        f.write(data.read())
        f.close()

    except (Exception, IOError) as error:
        print ("Error writing data to file! => ", error)
        sys.exit(3)

    # copy the current frame to the frames repository for the timelapse
    frame_num = 0
    while Path(current_path + "/frames/frame_" + f'{frame_num:03}' + ".jpg").exists():
        frame_num += 1
    frame_path = current_path + "/frames/frame_" + f'{frame_num:03}' + ".jpg"
    print("Frame:    ", frame_path)
    shutil.copyfile(snap_path, frame_path)

    # upload snapshot file to ftp
    ftp = FTPClient(
        config['FTP']['SERVER'], 
        config['FTP']['USER'], 
        config['FTP']['PASS'])
    ftp.connect()
    ftp.upFile(config['FTP']['BASE_DIR'], snap_path, snapshot)
    ftp.close()

    # upload snapshot file to B2
    b2Upload(
        config['B2']['app_key_id'],
        config['B2']['app_key'],
        config['B2']['bucket_name'],
        snap_path, snapshot)

    elapsed_time = datetime.now() - now
    print("\nElapsed Time: ", elapsed_time)
    print("=================================================")

    sys.exit(0)